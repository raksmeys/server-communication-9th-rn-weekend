// In App.js in a new project

import * as React from 'react';
import { View, Text, Button, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Icon } from 'native-base';
import ArticleList from './components/ArticleList';
import ArticleDetail from './components/ArticleDetail';
import AddArticle from './components/AddArticle';

export const baseURL = 'http://110.74.194.124:15011/'
export const header = {
  'Content-Type': 'Application/json',
  'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
}
const Stack = createStackNavigator()

function App(){
  return (

    <NavigationContainer>
      <Stack.Navigator initialRouteName="ArticleList">
        <Stack.Screen name="ArticleList" 
        options={({ navigation, route }) => ({
          headerRight: props => <Button 
            onPress={() => navigation.navigate('Add')}
            title="Add"
          />,
        })}
        component={ArticleList} />
        <Stack.Screen name="Detail" component={ArticleDetail} />
        <Stack.Screen name="Add" component={AddArticle} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}


export default App;