/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import AddArticle from './components/AddArticle';
import ArticleDetail from './components/ArticleDetail';
import ArticleList from './components/ArticleList';

AppRegistry.registerComponent(appName, () => App);
