
import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet, Dimensions, Image, ActivityIndicator, TextInput, SafeAreaView, TouchableOpacity, Alert} from 'react-native'
import {baseURL, header} from './../App'
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { LoadingView } from './LoadingView';

const W = Dimensions.get('screen').width
export default class Home extends Component {

    constructor(props){
        super(props)
        this.state = {
            id: 1,
            page: 1,
            totalPage: 1,
            articles: [],
            isLoading: true,
            refreshing: false,
            title: '',
        }
    }

    fetchNewData = () => {
        this.setState({page: 1, articles: []}, this.searchAndFetchArticle)
    } 
    fetchData = async () => {
        fetch(`${baseURL}v1/api/articles?page=${this.state.page}&limit=15`, {
            method: 'GET',
            headers: header
        })
        .then(res => res.json())
        .then(result => {
                this.setState({
                    articles: this.state.articles.concat(result.data),
                    totalPage: result.pagination.total_pages,
                    isLoading: false
                })
            
        })
        .catch(error => console.log(error))
    }

    searchAndFetchArticle = () => {
        if (this.state.title != ""){
            this.setState({articles: []})
        }
        fetch(`${baseURL}v1/api/articles?title=${this.state.title}&page=${this.state.page}&limit=15`, {
            method: 'GET',
            headers: header
        })
        .then(res => res.json())
        .then(result => {
                this.setState({
                    articles: this.state.articles.concat(result.data),
                    totalPage: result.pagination.total_pages,
                    isLoading: false
                })
            
        })
        .catch(error => console.log(error))
    }

    componentDidMount(){
        this.setState({isLoading: true}, this.searchAndFetchArticle)
        
        
    }
   
   

    renderFooter = () => {
        return (
            this.state.isLoading ? 

            <View>
                <ActivityIndicator size="large" />
            </View>
            : 
            null
        )
    }

    handleLoadMore = () => {
        this.state.page >= this.state.totalPage ? 
        this.setState({isLoading: false}) : this.setState({page: this.state.page + 1, isLoading: true}, this.searchAndFetchArticle)
    }
    seletedItem = (id) => {
        console.log('selected', id)
        this.props.navigation.navigate('Detail', {id: id})
    }

    deleteArticle = (id) => {

        Alert.alert(
            "Are you sure to delete",
            "Delete Article",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => 
            
                {
                    fetch(`${baseURL}v1/api/articles/${id}`, {
                        method: 'DELETE',
                        headers: header
                    })
                    .then(res => res.json())
                    .then(res => {
                        const filterArticle = this.state.articles.filter(res => res.id !== id)
                        this.setState({
                            articles: filterArticle
                        }, this.searchAndFetchArticle)

                        
                    })
                }
            
            }
            ],
            { cancelable: false }
          );
        
    }
    componentDidUpdate(prevProps, prevState){

        this.props.route.params !== undefined && prevState.articles.unshift(this.props.route.params.article)
     
    }

    render() {
        
        const {articles} = this.state

        console.log("article", articles)
        
        return (

            <SafeAreaView>
                
                <View>
                        <TextInput
                            style={styles.searchBarStyle} 
                            placeholder="Search Article"
                            onChangeText={(title) => this.setState({title}, this.searchAndFetchArticle)}
                        />
                </View>
                <View >
                   
                    {
                        articles.length == 0 ? <LoadingView />
                        : 
                        <FlatList 
                            showsVerticalScrollIndicator={false}
                            data={articles}
                            renderItem={({item, index}) => 
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('Detail', {id: item.id})}
                                    onLongPress={() => this.deleteArticle(item.id)}
                                >
                                        <Items 
                                        imageSource={item.image_url}
                                        title={item.title}
                                        desc={item.description}
                                        createdDate={item.created_date}
                                        />
                                </TouchableOpacity>
                            }
                            legacyImplementation={true}
                            onEndReachedThreshold={0}
                            refreshing={this.state.refreshing}
                            onRefresh={this.fetchNewData}
                            onEndReached={this.handleLoadMore}
                            ListFooterComponent={this.renderFooter}
                            keyExtractor={(item, index) => index.toString()}
                           
                        />
                    }
 
                </View>
            </SafeAreaView>
           
        )
    }
}

export function Items ({imageSource, title, desc, createdDate}){
    return (
        <View style={{flex: 1, flexDirection: 'row', backgroundColor: 'whitesmoke', margin: 5}}>
            <Image 
                style={{width: 110, height: 110}}
                source={{uri: imageSource == null ? 'https://images.pexels.com/photos/2236713/pexels-photo-2236713.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' : imageSource}}
            />
           <View style={{flex: 1, flexDirection: 'column'}}>
                <Text
                    numberOfLines={1}
                    ellipsizeMode="tail" 
                    style={{fontSize: 20, color: 'red', margin: 10}}>{title}</Text>
                <Text 
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={{color: 'darkgray', margin: 10}}>{desc}</Text>
                <Text 
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={{color: 'darkgray', margin: 10}}>{createdDate}</Text>
           </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        flex: 1,
        justifyContent: 'center',

    },
    containerCard: {
        marginTop: 5

    },
    searchBarStyle: {
        width: Dimensions.get('screen').width - 20,
        height: 60,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 15,
        marginTop: 15,
        marginBottom: 15,

        padding: 10
        
    }
})
