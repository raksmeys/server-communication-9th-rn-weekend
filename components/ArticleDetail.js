import React, { Component } from 'react'
import { Text, View, Image, Dimensions } from 'react-native'
import {baseURL} from './../App'

const W = Dimensions.get('screen').width
export default class ArticleDetail extends Component {
    constructor(props){
        super(props)
        this.state = {
            article: ''
        }
    }
    fetchOneArticle = (id) => {
        fetch(`${baseURL}v1/api/articles/${id}`, {
            method: 'GET',
            headers: {
                'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
        .then(res => res.json())
        .then(res => this.setState({
            article: res.data
        }))
    }
    componentDidMount(){
        this.fetchOneArticle(this.props.route.params.id)
    }
    render() {
        const {article} = this.state
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <Text> {this.props.route.params.id} </Text>
                <Image 
                    style={{width: W - 40, height: 200}}
                    source={{uri: article.image_url}}
                />
                <Text>{article.title}</Text>
                <Text>{article.description}</Text>
            </View>
        )
    }
}
