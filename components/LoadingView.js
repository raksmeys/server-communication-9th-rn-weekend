
import React from "react";
import { View } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

export const LoadingView = () => {
    return (
        <SkeletonPlaceholder>
        <View style={{ flexDirection: "row", alignItems: "center", margin: 10 }}>
          <View style={{ width: 100, height: 100, borderRadius: 5 }} />
          <View style={{ marginLeft: 20 }}>
            <View style={{ width: 200, height: 30, borderRadius: 4 }} />
            <View
              style={{ marginTop: 6, width: 180, height: 30, borderRadius: 4 }}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", margin: 10 }}>
          <View style={{ width: 100, height: 100, borderRadius: 5 }} />
          <View style={{ marginLeft: 20 }}>
            <View style={{ width: 200, height: 30, borderRadius: 4 }} />
            <View
              style={{ marginTop: 6, width: 180, height: 30, borderRadius: 4 }}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", margin: 10 }}>
          <View style={{ width: 100, height: 100, borderRadius: 5 }} />
          <View style={{ marginLeft: 20 }}>
            <View style={{ width: 200, height: 30, borderRadius: 4 }} />
            <View
              style={{ marginTop: 6, width: 180, height: 30, borderRadius: 4 }}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", margin: 10 }}>
          <View style={{ width: 100, height: 100, borderRadius: 5 }} />
          <View style={{ marginLeft: 20 }}>
            <View style={{ width: 200, height: 30, borderRadius: 4 }} />
            <View
              style={{ marginTop: 6, width: 180, height: 30, borderRadius: 4 }}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", margin: 10 }}>
          <View style={{ width: 100, height: 100, borderRadius: 5 }} />
          <View style={{ marginLeft: 20 }}>
            <View style={{ width: 200, height: 30, borderRadius: 4 }} />
            <View
              style={{ marginTop: 6, width: 180, height: 30, borderRadius: 4 }}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", margin: 10 }}>
          <View style={{ width: 100, height: 100, borderRadius: 5 }} />
          <View style={{ marginLeft: 20 }}>
            <View style={{ width: 200, height: 30, borderRadius: 4 }} />
            <View
              style={{ marginTop: 6, width: 180, height: 30, borderRadius: 4 }}
            />
          </View>
        </View>
      </SkeletonPlaceholder>
    );
};