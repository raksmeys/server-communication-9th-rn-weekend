import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, StyleSheet, Dimensions, Image, Button} from 'react-native'
import ImagePicker from "react-native-image-picker"

import {baseURL, header} from './../App'


const W = Dimensions.get('screen').width
export default class AddArticle extends Component {
    constructor(props){
        super(props)
        this.state = {
            title: '',
            desc: '',
            image_url: 'https://shahpourpouyan.com/wp-content/uploads/2018/10/orionthemes-placeholder-image-1.png',
            isLoading: false,
            status: 1
        }
    }

    selectImage = async () => {
        ImagePicker.launchImageLibrary({
            mediaType: 'photo',
            allowsEditing: true
        }, (response) => {
            if (response.didCancel){
                console.log("user did candcel")
            }else if (response.error){
                console.log("response error")
            }else {
                alert("is upload")
                this.setState({
                    isLoading: true,
                    image_url: response.uri
                })
                // upload image to server
                let file = new FormData()
                const name = response.type.substr(6, response.type.length)
                file.append('file', {type: response.type, uri: response.uri, name: name})

                fetch(`${baseURL}v1/api/uploadfile/single`,{
                    method: 'POST',
                    headers: header,
                    body: file
                })
                .then(res => res.json())
                .then(res => {
                    this.setState({
                        image_url: res.data,
                        isLoading: false
                    })
                })

            

            }
        })
    
    }

    uploadArticle = async () => {
        const article = {
            title: this.state.title,
            description: this.state.desc,
            image: this.state.image_url
        }

        await fetch(`${baseURL}v1/api/articles`, {
            method: 'POST',
            headers: header,
            body: JSON.stringify(article)
        })
        .then(res => res.json())
        .then(res => this.setState({
            article: res.data, 
            status: res.code
        }))
        
        if (this.state.status == 2222){
            this.props.navigation.navigate('ArticleList', {
                article: this.state.article
            })
        }else if (this.state.status == 9999){
            alert('falied')
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.selectImage}>
                    {
                        <Image 
                        source={{uri: this.state.image_url}}
                        style={{width: 200, height: 200, borderRadius: 10, margin: 10}}
                        />
                       
                    }
                </TouchableOpacity>
                <Button 
                    title="add"
                    onPress={this.selectImage}
                />
                <TextInput 
                    placeholder="title"
                    style={styles.inputStyle}
                    onChangeText={(title) => this.setState({title})}
                />
                <TextInput 
                    placeholder="description"
                    style={styles.inputStyle}
                    onChangeText={(desc) => this.setState({desc})}
                />
                <TouchableOpacity 
                    onPress={() => this.uploadArticle()}
                    style={styles.btn}>
                    <Text style={{color: '#fff'}}>Add</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center'
    },
    inputStyle: {
        width: W - 40,
        height: 70,
        borderRadius: 15,
        paddingLeft: 10,
        borderColor: 'lightgray',
        borderWidth: 1,
        margin: 10
    },
    btn: {
        width: W - 40,
        height: 70,
        borderRadius: 35,
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    }
})
